# tripsApi

register Trips

## Getting started
command:

```
cd existing_repo
git remote add origin https://gitlab.com/jpalma45/tripsapi.git
git branch -M main
git push -uf origin main
```

## Installation
Required docker, conected docker for your used.

## Usage
run:
 docker build -t node-api .
 docker run -it -p 5000:4000 node-api

## Support
johanpalmaperea68@gmail.com.

## License
For open source projects, say how it is licensed.
