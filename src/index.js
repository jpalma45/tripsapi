const mongoose = require('mongoose');
const app = require('./app');

app.set('port', process.env.PORT || 4000);

const main = async () => {
    await mongoose.connect('mongodb+srv://admin:admin@cluster0.3dxyb.mongodb.net/tripsdb?retryWrites=true&w=majority', { 
        useNewUrlParser: true,
        useUnifiedTopology: true })
          .then(() => {
                app.listen(app.get('port'), () => {
                    console.log(`Server on port: ${app.get('port')}`)
                });
        }).catch(err => console.log(err));
}

main();