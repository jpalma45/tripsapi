const tripService = require('../services');

const createTrip = async (req, res) => {
    res.status(201).json({
        message: 201,
        status: 'succes',
        data: await tripService.tripCreate({ ...req.body })
    })
}

const getTrips = async (req, res) => {
    res.status(200).json({
        message: 200,
        status: 'succes',
        data: await tripService.getTrips({ ...req.query })
    })
}

module.exports = {
    createTrip,
    getTrips
}