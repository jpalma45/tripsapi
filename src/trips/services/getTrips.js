const tripDao = require('../dao/index');

const getTrips = async (filterTrip) => {
    try {
        const filter = {};

        if (filterTrip?.start_gte) {
            filter.start_gte = {
                $match: {
                    "start.address": {$gte: String(filterTrip?.start_gte)}
                }
            }
        } else {
            filter.start_gte = {
                $match: {}
            }
        }
    
        if(filterTrip?.start_lte) {
            filter.start_lte = {
                $match: {
                    "start.address": {$lte: String(filterTrip?.start_lte)}
                }
            }
        }else {
            filter.start_lte = {
                $match: {}
            }
        }
        
        if (filterTrip?.distance_gte) {
            filter.distance = 
            {
                $match: {
                    distance: {$gte: filterTrip.distance_gte}
                }
            }
        } else {
            filter.distance =  {
                $match: {}
            }
        }
    
        const pagination = {
            limit: Number(filterTrip.limit) || 20,
            offset: Number(filterTrip.offset) || 0,
        }
        const response = await tripDao.getTrips(filter, pagination);
        return response;
    } catch (error) {
        throw new Error('Error in get trip');
    }
}

module.exports = getTrips;
