const tripCreate = require('./tripCreate');
const getTrips = require('./getTrips');

module.exports = {
    tripCreate,
    getTrips
}