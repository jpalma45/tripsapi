const moment = require('moment');
const tripDao = require('../dao/index');
const integrations = require('../../utils/integrations')

const createTrip = async (dataTrip) => {
    const { readings } = dataTrip;
    try {

        const dataCreateTrip = await getAddress({locationOne: { lat: readings[0].location.lat, lon: readings[0].location.lon }, locationTow: { lat: readings[readings.length-1].location.lat, lon: readings[readings.length-1].location.lon }, readings})
        const tripCreated = await tripDao.createTrips(dataCreateTrip);
        return tripCreated;
    } catch (error) {
        throw new Error('Error in create trip');
    }
}

const getAddress = async (address) => {
    try {
        const {dataOne, dataTwo} = await integrations.addressOneAndAddressTwo({latOne: address.locationOne.lat, lonOne: address.locationOne.lon, latTwo: address.locationTow.lat, lonTwo: address.locationTow.lon})
        const route = await integrations.addressDistanceTime({from:dataOne[0].name, to:dataTwo[dataTwo.length-1].name})
        const routeShape = await integrations.addressShape({from:dataOne[0].name, to:dataTwo[dataTwo.length-1].name})
        const shape = await integrations.boundingBox(routeShape);

        let countOverSpeed = 0;
        address.readings.forEach((reading) => {
            if (reading.speed > reading.speedLimit) {
                countOverSpeed = countOverSpeed + 1;
            }
        })

        const createTrip = {
            start: {
                time: moment().unix(),
                lat: dataOne[0].latitude,
                lon: dataOne[0].longitude,
                address: dataOne[0].name
            },
            end: {
                time: moment().unix(),
                lat: dataTwo[dataTwo.length-1].latitude,
                lon: dataTwo[dataTwo.length-1].longitude,
                address: dataTwo[dataTwo.length-1].name
            },
            distance: route.distance,
            duration: route.realTime,
            overspeedsCount: countOverSpeed,
            boundingBox: shape
            };

        return createTrip;
    } catch (error) {
        throw new Error('Error in created object for create trip');
    }
}

module.exports = createTrip;