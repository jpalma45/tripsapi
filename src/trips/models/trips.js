const { model, Schema } = require('mongoose');

const tripsSchema = new Schema({
    start: {
        type: Object,
        required: false,
    },
    end: {
        type: Object,
        required: false,
    },
    duration: {
        type: Number,
        required: false,
    },
    distance: {
        type: Number,
        required: false,
    },
    overspeedsCount: {
        type: Number,
        required: false,
    },
    boundingBox: {
        type: Array,
        required: false,
    }
},{
    timestamps: true
});

module.exports = model('trips', tripsSchema);