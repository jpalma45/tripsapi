const { Router } = require('express');
const router = Router();
const tripController = require('../controller')

router.post('/', tripController.createTrip);
router.get('/', tripController.getTrips);


module.exports = router;