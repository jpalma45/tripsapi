const { tripsModel } = require('../models');


const createTrips = (dataTrip) => tripsModel.create(dataTrip);

const getTrips = (filters, pagination) => tripsModel.aggregate([
    filters?.start_lte,
    filters?.start_gte,
    filters?.distance,
    {
        $skip: pagination.offset,
    },
    {
        $limit: pagination.limit,
    },
    {
        $project:{
            start: '$start',
            end: '$end',
            duration: '$duration',
            distance: '$distance',
            overSpeedsCount: '$overSpeedsCount',
            boundingBox: '$boundingBox',
            createdAt: '$createdAt',
            updatedAt: '$updatedAt'
        }
    }
])

module.exports = {
    createTrips,
    getTrips
}