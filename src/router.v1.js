const tripRouter = require('./trips/routes');

const routerV1 = (expressApp) => {
    expressApp.use('/api/v1/trip', tripRouter);
}

module.exports = routerV1;