const express = require('express');
const morgan = require('morgan');
const app = express();
const routerV1 = require('./router.v1');

app.use(express.json())
app.use(morgan('dev'));

routerV1(app);

module.exports = app;