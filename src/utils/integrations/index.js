const { default: axios } = require('axios');
const superagent = require('superagent');

const addressOneAndAddressTwo = async ({latOne, lonOne, latTwo, lonTwo}) => {
    try {
        const [addressOne, addressTwo] = await Promise.all([
            axios.get(`http://api.positionstack.com/v1/reverse?access_key=4d33651d0833f69d11f8d8d7bb65145c&query=${latOne},${lonOne}`),
            axios.get(`http://api.positionstack.com/v1/reverse?access_key=4d33651d0833f69d11f8d8d7bb65145c&query=${latTwo},${lonTwo}`)
            ]);
            const {data: {data: dataOne}} = addressOne;
            const {data: {data: dataTwo}} = addressTwo;
        
            return {
                dataOne,
                dataTwo
            }
    } catch (error) {
        throw new Error('Error find address')
    }
}

const addressDistanceTime = async ({from, to}) => {
    try {
        const addressDistance = await superagent.get(`http://www.mapquestapi.com/directions/v2/route`)
        .query({ key:'tcM9whlyhtaMW707RB52xSEKEYP9A8fA', from, to })
        const { route } = addressDistance.body;
        return route;
    } catch (error) {
        throw new Error('Error in get address distance')
    }

}


const addressShape = async ({from, to}) => {
    try {
        const Shape = await superagent.get(`http://www.mapquestapi.com/directions/v2/alternateroutes`)
        .query({ key:'tcM9whlyhtaMW707RB52xSEKEYP9A8fA', from, to, maxRoutes:2, timeOverage:100 })
        const { route: routeShape } = Shape.body;
        return routeShape;
    } catch (error) {
        throw new Error('Error in get address Shapes')
    }

}

const boundingBox = async (routeShape) => {
    try {
        const shape = [];
        const shapePoints = routeShape.shape.shapePoints;
        for (i=0; i<shapePoints.length; i++){ 
            if (!String(shapePoints[i]).startsWith('-')) {
                shape.push({
                    mapLat: shapePoints[i],
                    mapLng: shapePoints[i+1]
                })
            }
        }
        return shape;
    } catch (error) {
        throw new Error('Error in map shapes lat, lng')
    }
}

module.exports = {
    addressOneAndAddressTwo,
    addressDistanceTime,
    addressShape,
    boundingBox,
}
