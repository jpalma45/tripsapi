const chai =  require('chai');
const chaiHttp = require('chai-http');
const expect = require('chai').expect;

chai.use(chaiHttp);
const url = 'http://localhost:4000';

describe('get trips: ',()=>{
  describe('get all trips: ',()=>{
    it('should get all trips', (done) => {
      chai.request(url)
        .get('/api/v1/trip')
        .end( function(err,res){
        expect(res).to.have.status(201);
        expect(res.body.data).to.be.an('array');
      done();
    });
    });
  });

  describe('get limit trips: ',()=>{
    it('should get limit trips', (done) => {
      chai.request(url)
        .get('/api/v1/trip?limit=1')
        .end( function(err,res){
        expect(res).to.have.status(201);
        expect(res.body.data).to.be.length(1);
      done();
    });
    });
  });
});

describe('register trips: ',()=>{
  const trip = {
    "readings": [
      {
        "time": 1642500462000,
        "speed": 9,
        "speedLimit": 38,
        "location": {
          "lat": -33.580158,
          "lon": -70.567227
        }
      },
      {
        "time": 1642500466000,
        "speed": 26,
        "speedLimit": 38,
        "location": {
          "lat": -33.58013,
          "lon": -70.566995
        }
      },
      {
        "time": 1642500470000,
        "speed": 28,
        "speedLimit": 38,
        "location": {
          "lat": -33.580117,
          "lon": -70.566633
        }
      },
      {
        "time": 1642500474000,
        "speed": 13,
        "speedLimit": 38,
        "location": {
          "lat": -33.580078,
          "lon": -70.566408
        }
      },
      {
        "time": 1642500478000,
        "speed": 18,
        "speedLimit": 38,
        "location": {
          "lat": -33.580005,
          "lon": -70.566498
        }
      },
      {
        "time": 1642500482000,
        "speed": 32,
        "speedLimit": 38,
        "location": {
          "lat": -33.58002,
          "lon": -70.566837
        }
      },
      {
        "time": 1642500486000,
        "speed": 38,
        "speedLimit": 38,
        "location": {
          "lat": -33.580038,
          "lon": -70.567265
        }
      },
      {
        "time": 1642500490000,
        "speed": 38,
        "speedLimit": 38,
        "location": {
          "lat": -33.580043,
          "lon": -70.56773
        }
      },
      {
        "time": 1642500494000,
        "speed": 35,
        "speedLimit": 38,
        "location": {
          "lat": -33.580048,
          "lon": -70.56817
        }
      },
      {
        "time": 1642500498000,
        "speed": 20,
        "speedLimit": 38,
        "location": {
          "lat": -33.580053,
          "lon": -70.568502
        }
      }
    ]
  };
  describe('create trips: ',()=>{
    it('should create trips', (done) => {
      chai.request(url)
        .post('/api/v1/trip')
        .send(trip)
        .end( function(err,res){
      expect(res).to.have.status(201);
      done();
    });
    });
  });
});
 
 
